package tuto2;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

public class MyProducer {
	public static void main(String[] args) {
		new MyProducer();
	}

	MyProducer() {
		// records sent by this producer will have no key, and a Product value
		KafkaProducer<Void, Product> kafkaProducer;
		// create the Kafka producer with the appropriate configuration
		kafkaProducer = new KafkaProducer<>(configureKafkaProducer());
		try {
			// create and sent records to the 'test' topic
			Product p = new Product("apple", 3);
			kafkaProducer.send(new ProducerRecord<Void, Product>("test", null, p));
			p = new Product("banana", 3);
			kafkaProducer.send(new ProducerRecord<Void, Product>("test", null, p));
			p = new Product("carrot", 7);
			kafkaProducer.send(new ProducerRecord<Void, Product>("test", null, p));
			p = new Product("donuts", 6);
			kafkaProducer.send(new ProducerRecord<Void, Product>("test", null, p));
		} catch (Exception e) {
			System.err.println("something went wrong... " + e.getMessage());
		} finally {
			kafkaProducer.close();
		}
	}

	private Properties configureKafkaProducer() {
		Properties producerProperties = new Properties();
		producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.VoidSerializer");
		producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, tuto2.ProductSerializer.class);
		return producerProperties;
	}
}
