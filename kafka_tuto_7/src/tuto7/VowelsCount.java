package tuto7;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KGroupedStream;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.KeyValueMapper;
import org.apache.kafka.streams.kstream.Predicate;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.ValueMapper;
import org.apache.kafka.streams.kstream.ValueTransformer;
import org.apache.kafka.streams.kstream.ValueTransformerSupplier;
import org.apache.kafka.streams.processor.ProcessorContext;

public class VowelsCount {
	public static void main(final String[] args) {
		new VowelsCount();
	}

	public VowelsCount() {
		Topology vowelsCountTopology = createVowelsCountTopology();
		KafkaStreams vowelsCountStream = new KafkaStreams(vowelsCountTopology, configureVowelsCount());
		vowelsCountStream.start();
	}

	private Topology createVowelsCountTopology() {
		StreamsBuilder builder = new StreamsBuilder();

		// create a KStream from the input-topic
		KStream<String, String> source = builder.stream("input-topic");
		// materialize this KStream to the output-topic
		// source.to("output-topic");

		// create a KStream with upper-case records
		ValueTransformerSupplier<String, String> valueTransformerSupplier = new ValueTransformerSupplier<String, String>() {
			@Override
			public ValueTransformer<String, String> get() {
				return new ValueTransformer<String, String>() {
					@Override
					public void init(ProcessorContext context) {
						// empty on purpose
					}

					@Override
					public String transform(String value) {
						return value.toUpperCase();
					}

					@Override
					public void close() {
						// empty on purpose
					}
				};
			}
		};
		KStream<String, String> upperCaseStream = source.transformValues(valueTransformerSupplier);

		// materialize this KStream to the output-topic
		// upperCaseStream.to("output-topic");

		// create a KStream of characters
		ValueMapper<String, List<String>> valueMapper = new ValueMapper<String, List<String>>() {

			@Override
			public List<String> apply(String value) {
				List<String> list = new ArrayList<String>();
				for (int i = 0; i < value.length(); i++) {
					list.add(String.valueOf(value.charAt(i)));
				}
				return list;
			}
		};
		KStream charactersStream = upperCaseStream.flatMapValues(valueMapper);
		// charactersStream.to("output-topic");

		// keep only the vowels
		Predicate predicate = new Predicate<String, String>() {

			@Override
			public boolean test(String key, String value) {
				List<String> vowels = new ArrayList<String>();
				vowels.add("A");
				vowels.add("E");
				vowels.add("I");
				vowels.add("O");
				vowels.add("U");
				vowels.add("Y");
				return vowels.contains(value);
			}

		};
		KStream vowelsStream = charactersStream.filter(predicate);
		// vowelsStream.to("output-topic");

		// GROUP AND COUNT
		// First, we group records by value (which contains the letter):
		KeyValueMapper keySelector = new KeyValueMapper<String, String, String>() {

			@Override
			public String apply(String key, String value) {
				return value;
			}

		};
		KGroupedStream vowelsGroupedStream = vowelsStream.groupBy(keySelector);

		// Second, we feed a KTable which hold the count for each group:
		KTable vowelsCountTable = vowelsGroupedStream.count();
		// Each time the table gets updated, we want the updated row to be sent to a new
		// KStream:
		KStream vowelsCountingStreams = vowelsCountTable.toStream();
		// The types of the records in this KStream are
		// String for the key (since the key is now the letter, because of our
		// keySelector for the groupBy operation)
		// and Long for the value (since the value is the number of occurrences of the
		// letter).
		// When we want to materialize the record back to the output-topic, we need to
		// specify the new Serde:
		vowelsCountingStreams.to("output-topic", Produced.with(Serdes.String(), Serdes.Long()));

		/*
		// The value of vowelsCoutingStreams is of type Long, let's transform it to String
		// so the console consumer can keep printing it without changes
		ValueTransformerSupplier<Long, String> valueTransformerSupplier2 = new ValueTransformerSupplier<Long, String>() {
			@Override
			public ValueTransformer<Long, String> get() {
				return new ValueTransformer<Long, String>() {
					@Override
					public void init(ProcessorContext context) {
						// empty on purpose
					}

					@Override
					public String transform(Long value) {
						return String.valueOf(value);
					}

					@Override
					public void close() {
						// empty on purpose
					}
				};
			}
		};
		KStream v2 = vowelsCountingStreams.transformValues(valueTransformerSupplier2);
		v2.to("output-topic");
		*/
		return builder.build();
	}

	private Properties configureVowelsCount() {
		Properties vowelsCountStreamsProperties = new Properties();
		vowelsCountStreamsProperties.put(StreamsConfig.APPLICATION_ID_CONFIG, "vowelsCount");
		vowelsCountStreamsProperties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		// The semantics of caching is that data is flushed to the state store and
		// forwarded to
		// the next downstream processor node whenever the earliest of
		// commit.interval.ms or cache.max.bytes.buffering (cache pressure) hits.
		vowelsCountStreamsProperties.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
		vowelsCountStreamsProperties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG,
				Serdes.String().getClass().getName());
		vowelsCountStreamsProperties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG,
				Serdes.String().getClass().getName());
		vowelsCountStreamsProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		return vowelsCountStreamsProperties;
	}
}
