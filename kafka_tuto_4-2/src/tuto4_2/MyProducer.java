package tuto4_2;

import java.util.Properties;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

public class MyProducer {
	public static void main(String[] args) {
		new MyProducer();
	}

	MyProducer() {
		// records sent by this producer will have a numerical key, and a string value
		KafkaProducer<Integer, String> kafkaProducer;
		// create the Kafka producer with the appropriate configuration
		kafkaProducer = new KafkaProducer<>(configureKafkaProducer());
		try {
			// create and sent records to the 'test' topic
			int i = 0;
			String message = null;
			Integer key = 0;
			while (true) {
				message = "{ \"number\": " + i++ + "}";
				key = i % 3;
				Future<RecordMetadata> metadata = kafkaProducer
						.send(new ProducerRecord<Integer, String>("test", key, message));
				try {
					System.out.println(
							message + " with key " + key + " published on partition " + metadata.get().partition());
				} catch (Exception e) {
					System.out.println("error displaying metadata: " + e.getMessage());
				}

				// wait 1 second
				try {
					TimeUnit.SECONDS.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			System.err.println("something went wrong... " + e.getMessage());
		} finally {
			kafkaProducer.close();
		}
	}

	private Properties configureKafkaProducer() {
		Properties producerProperties = new Properties();
		producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.IntegerSerializer");
		producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringSerializer");
		producerProperties.put(ProducerConfig.METADATA_MAX_AGE_CONFIG, 10000);
		return producerProperties;
	}
}
